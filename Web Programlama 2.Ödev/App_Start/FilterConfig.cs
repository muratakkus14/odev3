﻿using System.Web;
using System.Web.Mvc;

namespace Web_Programlama_2.Ödev
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
