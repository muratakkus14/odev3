﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using Web_Programlama_2.Ödev.Models;

namespace Web_Programlama_2.Ödev.Site
{
    public class hepsiContext : DbContext
    {
        public hepsiContext()
            : base("hepsiContext")
            {
            }
        public DbSet<Duyurular> Duyurular { get; set; }
        public DbSet<Yorumlar> Yorumlar { get; set; }
        public DbSet<SatinAl> SatinAl { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}