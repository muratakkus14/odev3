﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Web_Programlama_2.Ödev.Models;

namespace Web_Programlama_2.Ödev.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult ChangeCulture(String lang)
        {
            if (lang != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lang);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(lang);
            }
            HttpCookie cookie = new HttpCookie("Language");
            cookie.Value = lang;
            Response.Cookies.Add(cookie);
            return View("Index");
        }

        // GET: Home
        EagleComputerEntities eagle = new EagleComputerEntities();
        public ActionResult Index()
        {
            ViewBag.Message = "";
            return View();
        }
        [AllowAnonymous]
        public ActionResult Ram()
        {
            ViewBag.Message = "Çeşitli markalarda,birçok özellikte ramler...";
            return View();
        }
        [AllowAnonymous]
        public ActionResult About()
        {
            ViewBag.Message = "Eagle Computer şirketi,tarihi,çalışma prensipleri ve çalışma ekibi hakkında merak edip, bilmek istediğiniz her şey...";
            return View();
        }
        [AllowAnonymous]
        public ActionResult Card()
        {
            ViewBag.Message = "Çeşitli markalarda,birçok özellikte Ekran Kartları...";
            return View();
        }
        [AllowAnonymous]
        public ActionResult Other()
        {
            ViewBag.Message = "Çeşitli markalarda donanım parçaları,aksesuarlar vs...";
            return View();
        }
        [AllowAnonymous]
        public ActionResult Processor()
        {
            ViewBag.Message = "Çeşitli markalarda,birçok özellikte İşlemciler...";
            return View();
        }
        [AllowAnonymous]
        public ActionResult SatinAl()
        {
            return View();
        }
    }

    public class AnasayfaDb
    {
        public List<Slider> slider { get; set; }
        public List<Duyurular> duyuru { get; set; }
        public List<Yorum> yorum { get; set; }
    }
}