﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Programlama_2.Ödev.Models;

namespace Web_Programlama_2.Ödev.Controllers
{
    public class PicturesController : Controller
    {
        private EagleComputerEntities db = new EagleComputerEntities();

        // GET: /Pictures/
        public ActionResult Index()
        {
            var resim = db.Urun.Include(p => p.urunID);
            return View(resim.ToList());   
        }

        // GET: /Pictures/Create
        public ActionResult Create()
        {
            return View();
        }
        //public ActionResult Create()
        //{
        //    return View();
        //}

        // POST: /Pictures/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UrunID,ozellik,fiyat,resim")] Urun resim)
        {
            if (ModelState.IsValid)
            {
                db.Urun.Add(resim);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UrunID = new SelectList(db.Urun, "UrunID", "resim", resim.urunID);
            return View(resim);
        }

        // GET: /Pictures/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Urun resim = db.Urun.Find(id);
            if (resim == null)
            {
                return HttpNotFound();
            }
            ViewBag.UrunID = new SelectList(db.Urun, "UrunID", "resim", resim.urunID);
            return View(resim);
        }

        // POST: /Pictures/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UrunID,ozellik,fiyat,resim")] Urun resim)
        {
            if (ModelState.IsValid)
            {
                db.Entry(resim).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UrunID = new SelectList(db.Urun, "UrunID", "resim", resim.urunID);
            return View(resim);
        }

        // GET: /Pictures/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Urun resim = db.Urun.Find(id);
            if (resim == null)
            {
                return HttpNotFound();
            }
            return View(resim);
        }

        // POST: /Pictures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Urun resim = db.Urun.Find(id);
            db.Urun.Remove(resim);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}