﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Programlama_2.Ödev.Models
{
    public class Duyurular
    {
        public int DuyurularID { get; set; }
        public string Dbaslik { get; set; }
        public string Dicerik { get; set; }
    }
}