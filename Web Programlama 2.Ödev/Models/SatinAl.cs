﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Programlama_2.Ödev.Models
{
    public class SatinAl
    {
        public int SatinAlID { get; set; }
        public string AdiSoyadi{ get; set; }
        public string Email { get; set; }
        public string Adres { get; set; }
        public int KrediKarti { get; set; }
    }
}