﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Programlama_2.Ödev.Models
{
    public class Yorumlar
    {
        public int YorumlarID { get; set; }
        public string isim { get; set; }
        public string Ybaslik { get; set; }
        public string Yicerik { get; set; }
    }
}